<?php

namespace MateriasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SesionController extends Controller
{    
    public function loginAction(Request $request){
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        
        if($error){
            //$this->addFlash('error', $error->messageKey());
        }
        else{
            $this->addFlash('aviso', 'Has iniciado sesión correctamente');
        }
        
        return $this->render('MateriasBundle:sesion:login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }
    
    public function logoutAction(Request $request){
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();
        return $this->redirectToRoute('inicio');
    }
    
    public function registrarAction(){
        return $this->render('MateriasBundle:sesion:registro.html.twig');
    }
    
    public function guardarRegistroAction(Request $request){
        
        $clave = $request->request->get('clave', null);
        
        $usuario = new \MateriasBundle\Entity\Usuario();        
        
        $usuario->nombre = $request->request->get('nombre', null);
        if (empty($usuario->getNombre())){
            $this->addFlash('error', 'Debes ingresar tu nombre');
            return $this->redirectToRoute('inicio');
        }
        
        $usuario->correo = $request->request->get('correo', null);
        if (empty($usuario->getCorreo())){
            $this->addFlash('error', 'Debes ingresar tu correo');
            return $this->redirectToRoute('inicio');
        }
        
        $usuario->codigo = $request->request->get('codigo', null);
        if (empty($usuario->getCodigo())){
            $this->addFlash('error', 'Debes ingresar tu código');
            return $this->redirectToRoute('inicio');
        }
        
        if (empty($clave)){
            $this->addFlash('error', 'Debes ingresar la clave');
            return $this->redirectToRoute('inicio');
        }
        
        $usuario->nivel_permisos = 'E';
        
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($usuario, $clave);
        $usuario->clave = $encoded;
        
        $usuario->fecha_edicion =  new \DateTime();
        $usuario->fecha_creacion = new \DateTime();
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($usuario);
        $em->flush();
        $this->addFlash('aviso', 'Te has registrado exitosamente. Ahora puedes iniciar sesión.');
        return $this->redirectToRoute('inicio');
    }
}
