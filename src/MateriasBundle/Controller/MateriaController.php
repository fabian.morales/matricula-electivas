<?php

namespace MateriasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MateriaController extends Controller
{
    public function indexAction() {
        $materias = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Materia')
                    ->findAll(); 
            
        return $this->render('MateriasBundle:materia:index.html.twig', ['materias' => $materias]);
    }
    
    public function listaProfesoresAction(){
        $profesores = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Usuario')
                    ->findBy(array('nivel_permisos' => 'P'));
        return $this->render('MateriasBundle:materia:lista_profesores.html.twig', ['profesores' => $profesores]);
    }
    
    public function listaMateriasProfesorAction($id){
        $profesor = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Usuario')
                    ->find($id);
        
        if (!sizeof($profesor)){
             $this->addFlash('error', 'Profesor no encontrado');
             return $this->redirectToRoute('inicio');
        }
        
        return $this->render('MateriasBundle:materia:lista_materias_profesor.html.twig', ['profesor' => $profesor]);
    }
    
    public function listaEstudiantesAction(){
        $estudiantes = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Usuario')
                    ->findBy(array('nivel_permisos' => 'E'));
        return $this->render('MateriasBundle:materia:lista_estudiantes.html.twig', ['estudiantes' => $estudiantes]);
    }
    
    public function listaMateriasEstudianteAction($id){
        $estudiante = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Usuario')
                    ->find($id);
        
        if (!sizeof($estudiante)){
             $this->addFlash('error', 'Estudiante no encontrado');
             return $this->redirectToRoute('inicio');
        }
        
        return $this->render('MateriasBundle:materia:lista_materias_estudiante.html.twig', ['estudiante' => $estudiante]);
    }
    
    public function misMatriculasAction(){
        $materias = $this->getUser()->getMatriculas();
        return $this->render('MateriasBundle:materia:lista_mis_materias.html.twig', ['materias' => $materias]);
    }
    
    public function matricularAction(){
        $materias = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Materia')
                    ->findAll();
        return $this->render('MateriasBundle:materia:form_matricular.html.twig', ['materias' => $materias]);
    }
    
    public function guardarMatriculaPostAction(Request $request){
        $id = $request->request->get('id_materia', '');
        return $this->guardarMatriculaAction($id);
    }
    
    public function guardarMatriculaAction($id){
        $materia = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Materia')
                    ->find($id);
        
        if (!sizeof($materia)){
            $this->addFlash('error', 'Electiva no encontrada');
            return $this->redirectToRoute('materias');
        }
        
        if ($materia->getCuposLibres() <= 0){
            $this->addFlash('error', 'Electiva no tiene cupos libres');
            return $this->redirectToRoute('user_matricula');
        }
        
        $usuario = $this->getUser();
        
        $ret = $usuario->getMatriculas()->filter(
            function($mat) use ($id) {
               return ($mat->getId() == (int)$id);
            }
        );
        
        if (count($ret)){
            $this->addFlash('error', 'Ya estás matriculado en esta electiva');
            return $this->redirectToRoute('user_matricula');
        }
        
        $usuario->adicionarMateria($materia);
        $em = $this->getDoctrine()->getManager();
        $em->persist($usuario);
        
        $materia->cupos_libres -= 1;
        $em->persist($materia);
        
        $em->flush();
        
        $this->addFlash('aviso', 'Te has matriculado exitosamente');
        return $this->redirectToRoute('user');
    }
    
    public function detalleMateriaAction($id){
        $materia = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Materia')
                    ->find($id);
        
        if (!sizeof($materia)){
             $this->addFlash('error', 'Electiva no encontrada');
             return $this->redirectToRoute('inicio');
        }
        
        return $this->render('MateriasBundle:materia:detalle_materia.html.twig', ['materia' => $materia]);
    }
}
