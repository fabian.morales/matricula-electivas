<?php

namespace MateriasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminUsuarioController extends Controller
{
    public function indexAction() {
        $usuarios = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Usuario')
                    ->findAll(); 
            
        return $this->render('MateriasBundle:admin_usuarios:index.html.twig', ['usuarios' => $usuarios]);
    }
    
    public function crearAction() {
        $usuario = new \MateriasBundle\Entity\Usuario();
       
        $tipos = [
            ["id" => "A", "nombre" => "Administrador"],
            ["id" => "E", "nombre" => "Estudiante"],
            ["id" => "P", "nombre" => "Profesor"],
        ];
       
       return $this->render('MateriasBundle:admin_usuarios:form.html.twig', ['usuario' => $usuario, 'tipos' => $tipos]);
    }
    
    public function editarAction($id) {
       $usuario = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Usuario')
                    ->find($id);
       
        if (!sizeof($usuario)){
             $this->addFlash('error', 'Usuario no encontrado');
             return $this->redirectToRoute('admin_user');
        }
       
        $tipos = [
            ["id" => "A", "nombre" => "Administrador"],
            ["id" => "E", "nombre" => "Estudiante"],
            ["id" => "P", "nombre" => "Profesor"],
        ];
       
        return $this->render('MateriasBundle:admin_usuarios:form.html.twig', ['usuario' => $usuario, 'tipos' => $tipos]);
    }
    
    public function guardarAction(Request $request) {     
        $id = $request->request->get('id', '');
        $clave = $request->request->get('clave', null);
        
        $usuario = null;
        if (!empty($id)){
            $usuario = $this->getDoctrine()
                        ->getRepository('MateriasBundle:Usuario')
                        ->find($id);
        }
        
        if (!sizeof($usuario)){
            $usuario = new \MateriasBundle\Entity\Usuario();
        }
        
        $usuario->nombre = $request->request->get('nombre', null);
        if (empty($usuario->getNombre())){
            $this->addFlash('error', 'Debe ingresar el nombre del usuario ');
            return $this->redirectToRoute('admin_user');
        }
        
        $usuario->correo = $request->request->get('correo', null);
        if (empty($usuario->getCorreo())){
            $this->addFlash('error', 'Debe ingresar el correo del usuario');
            return $this->redirectToRoute('admin_user');
        }
        
        $usuario->codigo = $request->request->get('codigo', null);
        if (empty($usuario->getCodigo())){
            $this->addFlash('error', 'Debe ingresar el código del usuario');
            return $this->redirectToRoute('admin_user');
        }
        
        $usuario->nivel_permisos = $request->request->get('tipo_usuario', null);
        
        if (!empty($clave)){
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($usuario, $clave);
            $usuario->clave = $encoded;
        }
        
        if (empty($clave) && empty($usuario->getId())){
            $this->addFlash('error', 'Debe ingresar la clave del usuario');
            return $this->redirectToRoute('admin_user');
        }
        
        if (empty($usuario->id)){
            $usuario->fecha_creacion = new \DateTime();
        }
        
        $usuario->fecha_edicion =  new \DateTime();
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($usuario);
        $em->flush();
        
        $this->addFlash('aviso', 'Usuario guardado exitosamente');
        return $this->redirectToRoute('admin_user');
    }
    
    public function borrarAction($id) {
        $usuario = $this->getDoctrine()
             ->getRepository('MateriasBundle:Usuario')
             ->find($id);
       
        if (!sizeof($usuario)){
            $this->addFlash('error', 'Usuario no encontrado');
            return $this->redirectToRoute('admin_user');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($usuario);
        $em->flush();

        $this->addFlash('aviso', 'Usuario borrado exitosamente');
        
        return $this->redirectToRoute('admin_user');
    }
}
