<?php

namespace MateriasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction() {
        return $this->render('MateriasBundle::index.html.twig');
    }
}
