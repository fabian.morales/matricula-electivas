<?php

namespace MateriasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminMateriaController extends Controller
{
    public function indexAction() {
        $materias = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Materia')
                    ->findAll(); 
                    
        return $this->render('MateriasBundle:admin_materias:index.html.twig', ['materias' => $materias]);
    }
    
    public function crearAction() {
        $materia = new \MateriasBundle\Entity\Materia();
        $profesores = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Usuario')
                    ->findBy(array('nivel_permisos' => 'P'));
       return $this->render('MateriasBundle:admin_materias:form.html.twig', ['materia' => $materia, 'profesores' => $profesores]);
    }
    
    public function editarAction($id) {
        $materia = $this->getDoctrine()
                     ->getRepository('MateriasBundle:Materia')
                     ->find($id);

        if (!sizeof($materia)){
            $this->addFlash('error', 'Materia no encontrada');
            return $this->redirectToRoute('admin_materia');
        }
       
        $profesores = $this->getDoctrine()
                    ->getRepository('MateriasBundle:Usuario')
                    ->findBy(array('nivel_permisos' => 'P'));
       
       return $this->render('MateriasBundle:admin_materias:form.html.twig', ['materia' => $materia, 'profesores' => $profesores]);
    }
    
    public function guardarAction(Request $request) {     
        $id = $request->request->get('id', '');
        
        $materia = null;
        if (!empty($id)){
            $materia = $this->getDoctrine()
                        ->getRepository('MateriasBundle:Materia')
                        ->find($id);
        }
        
        if (!sizeof($materia)){
            $materia = new \MateriasBundle\Entity\Materia();
        }
        
        $materia->nombre = $request->request->get('nombre', null);
        if (empty($materia->getNombre())){
            $this->addFlash('error', 'Debe ingresar el nombre de la materia');
            return $this->redirectToRoute('admin_user');
        }
        
        $materia->descripcion = $request->request->get('descripcion', '');
        $materia->cupos_libres = (int)$request->request->get('cupos_libres', null);
        $materia->id_profesor = (int)$request->request->get('id_profesor', null);
        
        if (empty($materia->id)){
            $materia->fecha_creacion = new \DateTime();
        }
        
        $materia->fecha_edicion =  new \DateTime();
        
        $profesor = $this->getDoctrine()
                        ->getRepository('MateriasBundle:Usuario')
                        ->find($materia->id_profesor);
        
        $materia->setProfesor($profesor);
                
        $em = $this->getDoctrine()->getManager();
        $em->persist($materia);
        $em->flush();
        
        $this->addFlash('aviso', 'Materia guardada exitosamente');
        
        return $this->redirectToRoute('admin_materia');
    }
    
    public function borrarAction($id) {
        $materia = $this->getDoctrine()
             ->getRepository('MateriasBundle:Materia')
             ->find($id);
       
        if (!sizeof($materia)){
            $this->addFlash('error', 'Materia no encontrada');
            return $this->redirectToRoute('admin_user');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($materia);
        $em->flush();

        $this->addFlash('aviso', 'Materia borrada exitosamente');
        return $this->redirectToRoute('admin_materia');
    }
}
