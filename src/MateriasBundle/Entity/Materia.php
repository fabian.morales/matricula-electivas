<?php

namespace MateriasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sis_mat_materia")
 */
class Materia
{
    public function __construct() {
        $this->profesor = new Usuario();
        $this->estudiantes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    public function getId(){
        return $this->id;
    }
    
    public function setId($valor){
        $this->id = $valor;
    }

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $nombre;
    public function getNombre(){
        return $this->nombre;
    }
    
    public function setNombre($valor){
        $this->nombre = $valor;
    }
    
    /**
     * @ORM\Column(type="string", length=500)
     */
    protected $descripcion;
    public function getDescripcion(){
        return $this->descripcion;
    }
    
    public function setDescripcion($valor){
        $this->descripcion = $valor;
    }
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $id_profesor;
    public function getIdProfesor(){
        return $this->id_profesor;
    }
    
    public function setIdProfesor($valor){
        $this->id_profesor = $valor;
    }
    
    /**
     * @ORM\Column(type="integer")
     */
    protected $cupos_libres;
    public function getCuposLibres(){
        return $this->cupos_libres;
    }
    
    public function setCuposLibres($valor){
        $this->cupos_libres = $valor;
    } 
    
    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $fecha_creacion;
    
    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $fecha_edicion;
    
    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="materias", fetch="EAGER")
     * @ORM\JoinColumn(name="id_profesor", referencedColumnName="id")
     */
    protected $profesor;
    public function getProfesor(){
        return $this->profesor;
    }
    
    public function setProfesor($valor){
        $this->profesor = $valor;
    }
    
    /** 
     * @ORM\ManyToMany(targetEntity="Usuario")
     * @ORM\JoinTable(name="sis_mat_matricula",
     *      joinColumns={@ORM\JoinColumn(name="id_materia", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id_estudiante", referencedColumnName="id")}
     *      )
     */
    protected $estudiantes;
    public function getEstudiantes(){
        return $this->estudiantes;
    }
    
    public function __get($property){ 
        return $this->$property; 
    }
    
    public function __set($property, $value){ 
        $this->$property = $value; 
    }
}