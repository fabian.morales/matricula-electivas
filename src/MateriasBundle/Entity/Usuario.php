<?php

namespace MateriasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sis_par_usuario")
 */
class Usuario implements UserInterface, \Serializable
{
    public function __construct() {
        $this->materias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->matriculas = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    public function getId(){
        return $this->id;
    }
    
    public function setId($valor){
        $this->id = $valor;
    }

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $nombre;
    public function getNombre(){
        return $this->nombre;
    }
    
    public function setNombre($valor){
        $this->nombre = $valor;
    }
    
    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $codigo;
    public function getCodigo(){
        return $this->codigo;
    }
    
    public function setCodigo($valor){
        $this->codigo = $valor;
    }
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $correo;
    public function getCorreo(){
        return $this->correo;
    }
    
    public function setCorreo($valor){
        $this->correo = $valor;
    }
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $clave;
    public function getClave(){
        return $this->clave;
    }
    
    public function setClave($valor){
        $this->clave = $valor;
    }
    
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $nivel_permisos;
    public function getNivelPermisos(){
        return $this->nivel_permisos;
    }
    
    public function setNivelPermisos($valor){
        $this->nivel_permisos = $valor;
    }    
    
    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $fecha_creacion;
    
    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $fecha_edicion;
        
    public function __get($property){ 
        return $this->$property; 
    }
    
    public function __set($property, $value){ 
        $this->$property = $value; 
    }
    
    public function getUsername(){
        return $this->codigo;
    }
    
    public function setUsername($valor){
        $this->codigo = $valor;
    }

    public function getRoles(){
        $roles = [
            'A' => 'ROLE_ADMIN',
            'U' => 'ROLE_USER',
            'E' => 'ROLE_USER',
            'P' => 'ROLE_USER'
        ];
        return array($roles[$this->nivel_permisos]);
    }

    public function getPassword(){
        return $this->clave;
    }
    
    public function setPassword($valor){
        $this->clave = $valor;
    }
    
    public function getSalt(){
    }
    
    public function eraseCredentials(){
    }
    
   /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->codigo,
            $this->clave,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->codigo,
            $this->clave,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
    
    /**
     * @ORM\OneToMany(targetEntity="Materia", mappedBy="profesor")
     */
    protected $materias;
    public function getMaterias(){
        return $this->materias;
    }
    
    /** 
     * @ORM\ManyToMany(targetEntity="Materia")
     * @ORM\JoinTable(name="sis_mat_matricula",
     *      joinColumns={@ORM\JoinColumn(name="id_estudiante", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id_materia", referencedColumnName="id")}
     *      )
     */
    protected $matriculas;
    public function getMatriculas(){
        return $this->matriculas;
    }
    
    public function adicionarMateria(Materia $materia){
        $this->matriculas[] = $materia;
    }

}