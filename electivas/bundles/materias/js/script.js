(function($, $window){
    $(document).ready(function() {
        $(document).ready(function() {
            $(window).resize(function () {
                if ($("#menu-resp").is(":visible")) {
                    $("#nav_menu").hide();
                } else {
                    $("#nav_menu").show();
                }
            });

            $("#menu_resp > a").click(function (e) {
                e.preventDefault();
                $("#nav_menu").toggle("slow");
            });
            
            $("a[rel='borrar-usuario']").click(function(e) {
                if(!confirm('¿Está seguro de borrar este usuario?')){
                    e.preventDefault();
                }
            });
            
            $("a[rel='borrar-materia']").click(function(e) {
                if(!confirm('¿Está seguro de borrar esta electiva?')){
                    e.preventDefault();
                }
            });
            
            $("a[rel='matricular']").click(function(e) {
                if(!confirm('¿Está seguro de matricularse en esta electiva?')){
                    e.preventDefault();
                }
            });
            
            $("#form_admin_materia").submit(function(e) {
                if ($("#nombre").val() === ""){
                    alert("Debe ingresar el nombre de la materia");
                    e.preventDefault();
                    return;
                }
                
                if ($("#id_profesor").val() === ""){
                    alert("Debe ingresar el nombre de la materia");
                    e.preventDefault();
                    return;
                }
            });
            
            $("#form_admin_usuario").submit(function(e) {
                if ($("#nombre").val() === ""){
                    alert("Debe ingresar el nombre del usuario");
                    e.preventDefault();
                    return;
                }
                
                if ($("#correo").val() === ""){
                    alert("Debe ingresar el correo del usuario");
                    e.preventDefault();
                    return;
                }
                
                if ($("#codigo").val() === ""){
                    alert("Debe ingresar el código del usuario");
                    e.preventDefault();
                    return;
                }
                
                if ($("#tipo_usuario").val() === ""){
                    alert("Debe seleccionar el tipo de usuario");
                    e.preventDefault();
                    return;
                }
            });
            
            $("#form_registro").submit(function(e) {
                if ($("#nombre").val() === ""){
                    alert("Debes ingresar tu nombre");
                    e.preventDefault();
                    return;
                }
                
                if ($("#correo").val() === ""){
                    alert("Debes ingresar tu correo");
                    e.preventDefault();
                    return;
                }
                
                if ($("#codigo").val() === ""){
                    alert("Debes ingresar tu código");
                    e.preventDefault();
                    return;
                }
                
                if ($("#clave").val() === ""){
                    alert("Debes ingresar tu clave");
                    e.preventDefault();
                    return;
                }
            });
            
            if ($.tooltipster !== 'undefined'){
                $('.tooltip-x').tooltipster({
                    theme: 'tooltipster-light'
                });
            }
        });
    });
})(jQuery, window);
