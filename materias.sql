-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.21-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla matriculas.sis_mat_materia
CREATE TABLE IF NOT EXISTS `sis_mat_materia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `descripcion` text,
  `id_profesor` int(11) DEFAULT NULL,
  `cupos_libres` int(11) DEFAULT '0',
  `fecha_creacion` timestamp NULL DEFAULT NULL,
  `fecha_edicion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__sis_par_usuario` (`id_profesor`),
  CONSTRAINT `FK__sis_par_usuario` FOREIGN KEY (`id_profesor`) REFERENCES `sis_par_usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla matriculas.sis_mat_materia: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `sis_mat_materia` DISABLE KEYS */;
INSERT INTO `sis_mat_materia` (`id`, `nombre`, `descripcion`, `id_profesor`, `cupos_libres`, `fecha_creacion`, `fecha_edicion`) VALUES
	(1, 'Matemáticas', 'Básicas y aritmética', 6, 10, '2017-06-04 03:44:20', '2017-06-04 03:44:20'),
	(2, 'Español', 'Español y literatura', 7, 15, '2017-06-04 03:44:00', '2017-06-04 03:44:00'),
	(3, 'Trigonometría', 'Geometría de triangulos', 6, 10, '2017-06-04 03:45:11', '2017-06-04 03:45:11');
/*!40000 ALTER TABLE `sis_mat_materia` ENABLE KEYS */;


-- Volcando estructura para tabla matriculas.sis_mat_matricula
CREATE TABLE IF NOT EXISTS `sis_mat_matricula` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_estudiante` int(11) DEFAULT NULL,
  `id_materia` int(11) DEFAULT NULL,
  `fecha_matricula` timestamp NULL DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT NULL,
  `fecha_edicion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_mat_sis_par_usuario` (`id_estudiante`),
  KEY `FK__sis_mat_materia` (`id_materia`),
  CONSTRAINT `FK__sis_mat_materia` FOREIGN KEY (`id_materia`) REFERENCES `sis_mat_materia` (`id`),
  CONSTRAINT `FK_mat_sis_par_usuario` FOREIGN KEY (`id_estudiante`) REFERENCES `sis_par_usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla matriculas.sis_mat_matricula: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `sis_mat_matricula` DISABLE KEYS */;
INSERT INTO `sis_mat_matricula` (`id`, `id_estudiante`, `id_materia`, `fecha_matricula`, `fecha_creacion`, `fecha_edicion`) VALUES
	(1, 2, 1, '2017-06-04 00:53:46', '2017-06-04 00:53:48', '2017-06-04 00:53:48'),
	(2, 2, 2, NULL, NULL, NULL),
	(3, 2, 3, NULL, NULL, NULL);
/*!40000 ALTER TABLE `sis_mat_matricula` ENABLE KEYS */;


-- Volcando estructura para tabla matriculas.sis_par_usuario
CREATE TABLE IF NOT EXISTS `sis_par_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(50) DEFAULT NULL,
  `nombre` varchar(100) NOT NULL DEFAULT '0',
  `correo` varchar(255) NOT NULL DEFAULT '0',
  `clave` varchar(255) NOT NULL DEFAULT '0',
  `nivel_permisos` enum('A','U','E','P') NOT NULL DEFAULT 'U',
  `fecha_creacion` timestamp NULL DEFAULT NULL,
  `fecha_edicion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `correo` (`correo`),
  UNIQUE KEY `codigo` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla matriculas.sis_par_usuario: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `sis_par_usuario` DISABLE KEYS */;
INSERT INTO `sis_par_usuario` (`id`, `codigo`, `nombre`, `correo`, `clave`, `nivel_permisos`, `fecha_creacion`, `fecha_edicion`) VALUES
	(1, '94064368', 'Administrador', 'admin@yopmail.com', '$2y$12$25XgUyhT5tZzbOhvnDP7EO/6Mb/pcyl6/jTghcW84cFRfv.Jwkmka', 'A', '2017-06-04 00:19:55', '2017-06-04 00:19:55'),
	(2, '123456', 'Fabian Morales', 'fabian.morales@outlook.com', '$2y$12$tGXPl8R0OdyFkYgFL7fxv.3VuzVIcafNLP7gDn1hevVeCPNqokrUq', 'E', '2017-06-04 07:47:20', '2017-06-04 07:47:20'),
	(6, '304462', 'Pepe Perez', 'pepe@yopmail.com', '$2y$12$zM9H4VqVVMucqL2TFWEI1.XaYHKTOPzXGcTooom./enSaLRQuNwTu', 'P', '2017-06-04 03:38:59', '2017-06-04 03:38:59'),
	(7, '7987000', 'Fercho Gomez', 'fercho@yopmail.com', '$2y$12$Y8HDRZrapKwM7isFz/DDJO2ktm71y7srUf4JHiJEANYmSqtByYUfm', 'P', '2017-06-04 03:39:30', '2017-06-04 03:39:30');
/*!40000 ALTER TABLE `sis_par_usuario` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
